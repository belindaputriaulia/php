<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>String PHP</title>
</head>
<body>
    <h1>Berlatih String PHP</h1>
    <?php   
        echo "<h3> Soal No 1</h3>";
        $first_sentence = "Hello PHP!" ;
        echo "Sentence: ";
        print ($first_sentence) . " <br>";
        echo "Panjang string: ";
        echo strlen ($first_sentence) . " <br>"; 
        echo "Jumlah kata: ";
        echo str_word_count ($first_sentence) . "<br><br>";

        $second_sentence = "I'm ready for the challenges";
        echo "Sentence: ";
        print ($second_sentence) . " <br>";
        echo "Panjang string: ";
        echo strlen ($second_sentence) . " <br>";
        echo "Jumlah kata: ";
        echo str_word_count ($second_sentence) . "<br><br>";

        
        echo "<h3> Soal No 2</h3>";
        $string2 = "I love PHP";
        echo "Sentence: ";
        print ($string2) . " <br>";
        echo "Kata pertama: " . substr($string2, 0, 1) . "<br>" ; 
        echo "Kata kedua: " . substr($string2, 2, 4);
        echo "<br> Kata Ketiga: " . substr($string2, 7, 3);

        echo "<h3> Soal No 3 </h3>";
        $string3 = "PHP is old but Good!";
        echo "String: \"$string3\" " . "<br>"; 
        echo str_replace ("Good!","awesome", $string3);
        // OUTPUT : "PHP is old but awesome"

    ?>
</body>
</html>
